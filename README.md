# Curso de Programación

## Tools
* [Docencia EIT](https://docencia-eit.udp.cl/app)
* [Editor de texto - VS Codium](https://vscodium.com/)
* [GCC / G++](https://www.bloodshed.net/)

# Programa del Curso
* [Programa del Curso](/resources/Programa.pdf)
* [Cronograma del Curso](/resources/planificacion/planificaci%C3%B3n%20progra.xlsx)

# Clases
1. Introducción al curso - [ODP](/resources/clases/p_1_introduccion.odp) | [PDF](/resources/clases/p_1_introduccion.pdf)
2. Varaibles - [ODP](/resources/clases/p_2_variables.odp) | [PDF](/resources/clases/p_2_variables.pdf)
3. Condiciones - [ODP](/resources/clases/p_3_condiciones.odp) | [PDF](/resources/clases/p_3_condiciones.pdf)


